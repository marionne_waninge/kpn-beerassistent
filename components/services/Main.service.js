(function() {

    'use strict';

    angular
        .module('boilerplate')
        .factory('MainService', MainService);

    MainService.$inject = ['$http', '$base64'];


    function MainService($http, $base64) {
        var API_URL =  'http://kpndigital.nl/beerapi/';
        var auth;
        var headers;

        return {
            getBeers: getBeers,
            postBeer: postBeer,
            getOrders: getOrders,
            updateOrder: updateOrder
        };

        function getAuth(){
            auth = $base64.encode('Marionne:Olifant87!');
            $http.defaults.headers.common['Authorization'] = 'Basic ' + auth;
        }

        function getBeers() {
            getAuth();
            return $http.get(API_URL + 'beers', {headers: headers});
        }

        function postBeer(beer){
            getAuth();

            var beerJSON = JSON.stringify(beer);
            return $http.post(API_URL + 'beers', beerJSON);
        }

        function getOrders() {
            getAuth();
            return $http.get(API_URL + 'orders', {headers: headers});
        }

        function updateOrder(order){
            getAuth();
            return $http.put(API_URL + 'order-update/' + order.orders[0].id, order.orders[0], {headers: headers} )
        }

    }

})();
