(function() {

    angular
        .module('boilerplate')
        .controller('OrderController', OrderController);

    OrderController.$inject = ['MainService'];

    /**
     * Order Controller
     *
     * @param
     * @param
     * @constructor
     * @ngInject
     */

    function OrderController(MainService) {
        var vm = this;
        vm.updateOrder = updateOrder;
        vm.orders;

        function updateOrder(beer){
            beer.orders[0].poured = true;

            console.log(beer);
            MainService.updateOrder(beer)
                .then(function(response){
                    console.log('updated');
                })
        }


        setInterval(function(){
            MainService.getOrders()
                .then(function(response){
                    if(response){
                        console.log(response);
                        vm.orders = response.data.results;
                    }
                })
        }, 2000);

    }
})();
