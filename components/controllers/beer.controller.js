(function() {

    angular
        .module('boilerplate')
        .controller('BeerController', BeerController);

    BeerController.$inject = ['MainService'];

    /**
     * Order Controller
     *
     * @param
     * @param
     * @constructor
     * @ngInject
     */

    function BeerController(MainService) {
        var vm = this;
        vm.postBeer = postBeer;
        vm.beer = {
            'name': '',
            'description': '',
            'category': '',
            'price_category': null,
            'image': '',
            'stars': null,
            'alcohol_percentage': ''
        };

        MainService.getBeers()
            .then(function(response){
                if(response){
                    vm.beers = response;
                }
            })
            .catch(function(error){
                console.log(error);
            });

        function postBeer(beer){
            MainService.postBeer(beer)
                .then(function(response){
                    console.log(response);
                });
        }
    }
})();
