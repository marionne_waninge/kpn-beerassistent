/**
 * 
 * AngularJS Boilerplate
 * @description           Description
 * @author                Jozef Butko // www.jozefbutko.com/resume
 * @url                   www.jozefbutko.com
 * @version               1.1.7
 * @date                  March 2015
 * @license               MIT
 * 
 */
;(function() {


  /**
   * Definition of the main app module and its dependencies
   */
  angular
    .module('boilerplate', [
      'ngRoute', 'base64'
    ])
    .config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$routeProvider', '$locationProvider'];

  /**
   * App routing
   *
   * You can leave it here in the config section or take it out
   * into separate file
   * 
   */
  function config($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(false);

    // routes
    $routeProvider
      .when('/', {
        templateUrl: 'components/views/home.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/orders', {
        templateUrl: 'components/views/orders.html',
        controller: 'OrderController',
        controllerAs: 'order'
      })
      .when('/beers', {
          templateUrl: 'components/views/beers.html',
          controller: 'BeerController',
          controllerAs: 'beer'
      })
    .when('/customer', {
        templateUrl: 'components/views/customer.html',
        controller: 'BeerController',
        controllerAs: 'beer'
    })
      .otherwise({
        redirectTo: '/'
      });

  }


  /**
   * Run block
   */
  angular
    .module('boilerplate')
    .run(run);

  run.$inject = ['$rootScope', '$location'];

  function run($rootScope, $location) {

    // put here everything that you need to run on page load

  }


})();